from  __future__ import print_function, division
import sys, time

def bin_search(L, needle):
    i_min = 0
    i_max = len(L)-1
    #i_mid = i_max//2
    while i_max >= i_min:
        i_mid = (i_min + i_max)//2
        if needle < L[i_mid]:
            i_max = i_mid-1
            #print("imax:" + str(i_max))
        elif needle > L[i_mid]:
            i_min = i_mid+1
        elif L[i_mid] == needle:
            return i_mid
    return -1


if __name__ == "__main__":
    in_file = "test.txt"
    if len(sys.argv) > 1:
        in_file = sys.argv[1]

    INPUT = list()
    with open(in_file, 'r') as f:
        for line in f:
            INPUT.append(line)

    NUMBERS = [int(x) for x in INPUT[2].split()]
    #print("NUMB:" + str(NUMBERS))
    NEEDLES = [int(x) for x in INPUT[3].split()]
    #print("NEEDLES:" +str(NEEDLES))
    
    INDEXES = [bin_search(NUMBERS, needl) for needl in NEEDLES]

    OUT_STR = ""
    for n in [a+1 if a >= 0 else a for a in INDEXES]:
        OUT_STR += str(n) + " "
    print(OUT_STR[:-1])


