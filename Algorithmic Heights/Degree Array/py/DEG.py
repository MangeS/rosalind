from  __future__ import print_function, division
import sys, time

def vertex_deg(edge_list):
    vert_degr = [0]*(edge_list[0][0]+1)

    for edge in edge_list[1:]:
        vert_degr[edge[0]] += 1
        vert_degr[edge[1]] += 1
    return vert_degr[1:]


if __name__ == "__main__":
    in_file = "test.txt"
    if len(sys.argv) > 1:
        in_file = sys.argv[1]

    INPUT = list()
    with open(in_file, 'r') as f:
        for line in f:
            INPUT.append([int(n) for n in line.split()])

    DEGREES = vertex_deg(INPUT)
    OUT_STR = ""
    for nums in DEGREES:
        OUT_STR += str(nums)
        OUT_STR += " "

    print(OUT_STR[:-1])


