from  __future__ import print_function, division
import sys, time

def vertex_deg(edge_list):
    vert_degr = [0]*(edge_list[0][0]+1)
    adj_list = [set() for _ in vert_degr]
    n_deg_sum = [0]*(edge_list[0][0]+1)
    for edge in edge_list[1:]:
        vert_degr[edge[0]] += 1
        vert_degr[edge[1]] += 1
        
        adj_list[edge[0]].add(edge[1])
        adj_list[edge[1]].add(edge[0])

    for i in xrange(len(adj_list)):
        for neighb in adj_list[i]:
            n_deg_sum[i] += vert_degr[neighb]

    return n_deg_sum[1:]


if __name__ == "__main__":
    in_file = "test.txt"
    if len(sys.argv) > 1:
        in_file = sys.argv[1]

    INPUT = list()
    with open(in_file, 'r') as f:
        for line in f:
            INPUT.append([int(n) for n in line.split()])

    DEGR_SUM = vertex_deg(INPUT)

    OUT_STR = ""

    for num in DEGR_SUM:
        OUT_STR += str(num) + " "

    print(OUT_STR)

