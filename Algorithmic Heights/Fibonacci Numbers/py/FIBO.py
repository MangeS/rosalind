import sys


if __name__ == "__main__":
    fib_n = 30
    if len(sys.argv) > 1:
        fib_n = int(sys.argv[1])

    def m_fib(num):
        fib_1 = 0
        fib_2 = 1
        for _ in xrange(num):
            fib_1, fib_2 = fib_2, fib_1+fib_2
        return fib_1


    print("fib(" + str(fib_n) +  ")=" + str(m_fib(fib_n)))
