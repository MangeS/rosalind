#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "inssort.h"

static int const line_l_max = 1000000;

int num_swaps_insert_srt(int numbers[], int n)
{
  int swaps = 0;
  int k = 0;
  
  for (int i=1;i<n;i++)
  {
    k = i;
    while (k>0 && numbers[k] < numbers[k-1])
    {  
      swap_int(&numbers[k-1], &numbers[k]);
      swaps++;
      k--;
    }
  }
  return swaps;
}

void print_array(int arr[], int l)
{
  int i;
  printf("[");
  for (i=0;i<l-1;i++)
  {
    printf("%d ", arr[i]);
  }
  printf("%d]\n", arr[i]);
}

int read_array(FILE *in_f, int *numbers)
{
  int reached_eof = 0;
  char tmp[5];//assumes longest numbr == 1000
  int i = 0, n=0;
  int c;
  while (!reached_eof)
  {
    while ((c=getc(in_f)) != ' ')
    {
      if (c == EOF)
      {
	reached_eof = 1;
	break;
      }else
      {
	tmp[i] = c;
      }
      i++;
    }   
    tmp[i]='\0';
    numbers[n] = atoi(tmp);
    i = 0;
    n++;
  }
  return n;
}

void swap_int(int* a, int* b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

int main(int argc, char *argv[])
{
  FILE *in_fp;
  char *in_file = "test.txt";

  char s[150];

  //get input file if arg given
  if (argc > 1)
  {
    in_file = argv[1];
  }

  if ((in_fp = fopen(in_file, "r")) == NULL)
  {
    printf("can't open file:%s\n", in_file);
    exit(EXIT_FAILURE);
  }

  fgets(s, 150, in_fp);


  //Num of numbers in number list
  int s_len = atoi(s);
  printf("len array:%d\n", s_len);

  int *num_list;
  num_list = (int*) malloc(s_len*sizeof(int));

  if (s_len != read_array(in_fp, num_list))
  {
    exit(EXIT_FAILURE);
  }
  

  //print_array(num_list, s_len);
  int sw = num_swaps_insert_srt(num_list, s_len);
  //print_array(num_list, s_len);
  printf("num swaps:%d\n", sw);
  
  fclose(in_fp);
  free(num_list);
}
