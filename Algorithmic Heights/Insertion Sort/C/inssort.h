#ifndef INSSORT_H
#define INSSORT_H

int 
get_line
(
 char s[], 
 int lim);

int 
num_swaps_insert_srt
(
 int [],
 int
 );

void 
print_array
(
 int [], 
 int
);

int
read_array
(
 FILE *in,
 int *numbers
 );

void 
swap_int
(
 int*,
 int* b
 );

#endif
