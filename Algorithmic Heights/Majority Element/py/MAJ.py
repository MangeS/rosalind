from  __future__ import print_function, division
import sys, time

def maj_elem(L):
    counts = [0]*10**5

    for n in L:
        counts[n] += 1

    for i, cnt in enumerate(counts):
        if cnt > len(L)/2:
            return i
    return -1


if __name__ == "__main__":
    in_file = "test.txt"
    if len(sys.argv) > 1:
        in_file = sys.argv[1]

    INPUT = list()
    with open(in_file, 'r') as f:
        for line in f:
            INPUT.append(line)

    OUT_STR = ""
    for nums in INPUT[1:]:
        OUT_STR += str(maj_elem([int(x) for x in nums.split()]))
        OUT_STR += " "

    print(OUT_STR[:-1])


