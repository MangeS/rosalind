from  __future__ import print_function, division
import sys


if __name__ == "__main__":
    in_file = "test.txt"
    
    if len(sys.argv) > 1:
        in_file = sys.argv[1]

    MAX_TAG = ""
    MAX_GC = 0.0

    with open(in_file, "r") as f:
        curr_tag = ""
        num_gc = 0
        num_other = 1
        for line in f:
            if line[0] == '>':
                if MAX_GC < num_gc/(num_gc+num_other):
                    MAX_GC = num_gc/(num_gc+num_other)
                    MAX_TAG = curr_tag
                curr_tag = line[1:-1]
                num_gc = 0
                num_other = 0                    
            else:
                for ch in line[:-1]:
                    if ch == 'G' or ch == 'C':
                        num_gc += 1
                    else:
                        num_other += 1
    if MAX_GC < num_gc/(num_gc+num_other):
        MAX_GC = num_gc/(num_gc+num_other)
        MAX_TAG = curr_tag

    print(MAX_TAG + "\n" + str(round(MAX_GC*100, 6)))
