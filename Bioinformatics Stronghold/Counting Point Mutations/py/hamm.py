from  __future__ import print_function, division
import sys


if __name__ == "__main__":
    in_file = "test.txt"
    
    if len(sys.argv) > 1:
        in_file = sys.argv[1]

    HAMM = 0
    HAMM_STR = list()

    with open(in_file, "r") as f:
        for line in f:
            HAMM_STR.append(line)

    for pair in zip(HAMM_STR[0], HAMM_STR[1]):
        if pair[0] != pair[1]:
            HAMM += 1
    print(str(HAMM))
