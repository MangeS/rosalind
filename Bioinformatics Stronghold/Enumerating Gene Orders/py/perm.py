from  __future__ import print_function, division
import itertools, sys


if __name__ == "__main__":
    num = 3
    
    if len(sys.argv) > 1:
        num = int(sys.argv[1])

    PERMS = [a for a in itertools.permutations(xrange(1,num+1), num)]
    print(str(len(PERMS)))

    for p in PERMS:
        tmp = ""
        for n in xrange(num):
            tmp += str(p[n]) + " "
        print(tmp[:-1])
        
        
