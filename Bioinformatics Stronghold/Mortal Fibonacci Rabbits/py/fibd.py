from  __future__ import print_function, division
import sys

def get_prev(l, ix):
    if ix < 0:
        return 0
    else:
        return l[ix]

def step(newborn, life):
    curr = len(newborn) - 1
    newb_next = 0

    for i in xrange(1, life):
        newb_next += get_prev(newborn, curr-i)

    newborn.append(newb_next)


if __name__ == "__main__":
    months = 6
    life = 3
    if len(sys.argv) > 1:
        months = int(sys.argv[1])
    if len(sys.argv) > 2:
        life = int(sys.argv[2])


    NEWBORN = [0, 1]

    for _ in xrange(len(NEWBORN)-1, months):
        step(NEWBORN, life)

    print("Num Wabbits:" + str(sum(NEWBORN[-life:])))

